(defproject tubular "lein-git-inject/version"
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [org.clojure/clojurescript "1.11.60"
                  :exclusions [com.google.javascript/closure-compiler-unshaded
                               org.clojure/google-closure-library
                               org.clojure/google-closure-library-third-party]]
                 [org.clojure/core.match "1.0.0"]
                 [thheller/shadow-cljs "2.19.8"]
                 [com.taoensso/timbre "5.2.1"]
                 [reagent "1.1.1"]
                 [bulma-cljs "0.1.4"]
                 [re-frame "1.2.0"]
                 [bidi "2.1.6"]
                 [com.cognitect/transit-cljs "0.8.280"]
                 [com.cognitect/transit-clj "1.0.329"]
                 [kibu/pushy "0.3.8"]]

  :npm-dev-deps {"shadow-cljs" "2.11.21"}

  :plugins [[cider/cider-nrepl "0.26.0"]
            [day8/lein-git-inject "0.0.15"]
            [lein-shadow "0.3.1"]
            [lein-shell "0.5.0"]
            [lein-ancient "1.0.0-RC3"]]

  :middleware [leiningen.git-inject/middleware]
  :git-inject {:version-pattern #"v([0-9]+\.[0-9]+(-[.*])?)$"}

  ;; Note: by default, lein will change the version in project.clj when you do a `lein release`.
  ;; To avoid this (because you now want the version to come from the git context at build time),
  ;; explicitly include the following steps to avoid using the default release process provided by lein.
  :release-tasks [["build-data"]
                  ["vcs" "assert-committed"]
                  ["deploy"]]

  :min-lein-version "2.9.0"

  :source-paths ["src/cljc" "src/cljs" "resources"]
  :test-paths ["test/cljc"]
  :data-path "data"

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]

  :shadow-cljs {:nrepl {:port 8777}

                :builds {:app {:target :browser
                               :output-dir "resources/public/js/compiled"
                               :asset-path "/js/compiled"
                               :modules {:app {:init-fn tubular.core/init
                                               :preloads [devtools.preload]}}

                               :devtools {:http-root "resources/public"
                                          :http-port 8280}
                               :release {:compiler-options
                                         {:closure-defines
                                          {tubular.version "lein-git-inject/version"}}}}
                         :test {:target    :node-test
                                 :output-to "out/node-tests.js"
                                 :ns-regexp "-test$"
                                 :autorun   true}}}

  :shell {:commands {"karma" {:windows         ["cmd" "/c" "karma"]
                              :default-command "karma"}
                     "open"  {:windows         ["cmd" "/c" "start"]
                              :macosx          "open"
                              :linux           "xdg-open"}}}

  :aliases {"watch"        ["with-profile" "dev" "do"
                            ["shadow" "watch" "app" "browser-test" "karma-test"]]

            "release"      ["with-profile" "prod" "do"
                            ["shadow" "release" "app"]]

            "build-report" ["with-profile" "prod" "do"
                            ["shadow" "run" "shadow.cljs.build-report" "app" "target/build-report.html"]
                            ["shell" "open" "target/build-report.html"]]

            "ci"           ["with-profile" "prod" "do"
                            ["test"]
                            ["shadow" "compile" "test"]
                            ;; ["shadow" "compile" "karma-test"]
                            ;; ["shell" "karma" "start" "--single-run" "--reporters" "junit,dots"]
                            ]
            "build-data"   ["run" "-m" "tubular.build.data/build"]}

  :profiles {:dev
             {:dependencies [[binaryage/devtools "1.0.6"]]
              :source-paths ["dev"]}

             :prod {}}

  :prep-tasks [])
