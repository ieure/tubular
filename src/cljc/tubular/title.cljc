;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.title
  (:require [clojure.core.match :refer [match]]
            [clojure.spec.alpha :as s]
            [tubular.config :as config]
            [tubular.spec.db :as sdb]
            [tubular.spec.tube :as stube]))

(defn- title-dispatch "Dispatch fn for computing page title."
  [panel]
  (match (s/conform ::sdb/panel panel)
         ::s/invalid :default
         [t & _] t))

(s/fdef title-dispatch
  :args (s/cat :panel ::sdb/panel)
  :ret keyword?)

(defmulti title "Return page title based on the panel."
  title-dispatch)

(defmethod title :default [& _]
  config/app-name)

(defmethod title :tube [[_ {:keys [::stube/desig]}]]
  (str config/app-name config/title-sep desig))

(defmethod title :tube-xref [[_ {:keys [::stube/desig]}]]
  (str config/app-name config/title-sep desig config/title-sep "Cross-reference"))
