;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2021 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.tube.size
  (:require [tubular.math :as math]))

(declare ->Size)

(defn max-delta "Return the max allowable delta between tube sizes, in mm"
  ([a b]      (max-delta (max (:size a) (:size b))))
  ([max-size] (math/log max-size)))

(defprotocol Sized
  "A thing with a size"

  (->mm            [this]       "Convert to millimeters.")
  (->in            [this]       "Convert to viewable inches.")
  (->viewable      [this]       "Convert to viewable size (approximate). May change units.")
  (->face          [this]       "Convert to face size. May change units.")
  (describe        [this]       "Describe this size.")
  (describe-inches [this]       "Describe this size in viewable inches.")
  (compat?         [this other] "Are two sizes the same?"))

(defn- any->in "Convert a number & unit to inches."
  [size unit]
  (let [in (condp = unit
             :in size
             :mm (/ size 25.4)
             :cm (/ size 2.54)
             true 666.666)
        log (math/log size)
        c (/ log 75)
        cin (- in c)
        rin (math/round cin)]
    #_(println size " " unit " "
             "(log= " log ")"
             "(c=" c ") "
             "-> " in " in "
             "-> " cin " cin "
             "-> " rin " rin"
             )
    rin))

(defrecord Size [size unit type]
  Sized

  (describe [this]
    (str size
         (if (= unit :in) "\"" (subs (str unit) 1))
         " "
         (subs (str type) 1)))

  (describe-inches [this]
    (str (-> this ->in ->viewable :size) "\""))

  (->mm [this]
    (if (= unit :mm)
      this
      (let [mm (condp = unit
                 :cm (* 10 size)
                 :in (* 25.4 size)
                 true 666.666)]
        (->Size (int mm) :mm type))))

  (->in [this]
    (if (and (= unit :in))
      this
      (->Size (any->in size unit) :in type)))

  (->viewable [this]
    (if (= type :viewable)
      this
      (->Size (- (any->in size unit) 1) :in type)))

  (compat? [this other]
    (let [sa (-> this  ->in ->viewable :size)
          sb (-> other ->in ->viewable :size)]
      (= sa sb))))
