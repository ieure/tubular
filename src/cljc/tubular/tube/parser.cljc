;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2013-2017, 2021, 2022 Ian Eure
;; Author: Ian Eure <ian.eure@gmail.com>
;;
(ns tubular.tube.parser
  (:require [clojure.string :as string]
            [taoensso.timbre :as timbre
             #?(:clj :refer
                :cljs :refer-macros)
             [warn warnf]]
            [tubular.phosphor :as phos]
            [tubular.tube.size :refer [->Size]]))

(defn str->int [^String s]
  #?(:clj  (Integer/parseInt s)
     :cljs (js/parseInt s)))

(defonce trailing-junk #"(-[RGB]| \([RGB]\))$")

(defn cleanup [^String text]
  (when-not (string? text)
    (warnf "input `%s' is a %s, not string" text (type text)))
  (-> (str text)
      (string/trim)
      (string/replace trailing-junk "")
      (string/upper-case)))

(defonce systems
  {:wtds "WTDS"
   :eia "EIA"
   :eiaj "EIAJ"
   :pro-electron "Pro Electron"
   :chinese "China"
   :tesla-ecimex "Tesla Ecimex"})

(defonce regions
  {:usa "USA"
   :japan "Japan"
   :europe "Europe"
   :china "China"
   :czech-republic "Czech Republic"})



(defonce wtds-applications
  {"A" "Direct view TV"
   "B" "Direct view TV, remanufactured / B quality"
   "M" "Direct view monitor"
   "S" "Unknown"
   "W" "Widescreen direct view TV"})

(defn- wtds-region [family]
  (condp = (subs family 0 1)
    "A" :usa
    "B" :usa
    "C" :usa
    "D" :usa
    "E" :europe
    "F" :europe
    "G" :europe
    "H" :europe
    "J" :japan
    "K" :japan
    "L" :japan
    "M" :japan
    nil))

(def wtds-re #"^([ABMSW]|VB)([0-9]{2,3})([A-Z]{2,3})([0-9]{1,3})?([WX])?([0-9]{1,3})?")

(defn wtds [^String text]
  (if-let [[_ application size family family-member phosphor yoke] (re-find wtds-re text)]
    (merge {:system :wtds
            :application (get wtds-applications application "Unknown")
            :size (->Size (str->int size) :cm :viewable)
            :family family}
           (when phosphor {:phosphor (phos/normalize phosphor)})
           (if-let [region (wtds-region family)]
             {:region region})
           (when family-member {:family-member family-member})
           (when yoke {:yoke yoke}))))



(def ^:private eia-re #"^([PM]*)([0-9]{1,2})(V?)([A-Z]+)([BP][0-9]+|TC[0-9]+)([A-Z]?)")

(defn eia [^String text]
  (if-let [[_ _ size viewable? family phosphor-or-yoke revision] (re-find eia-re text)]
    (merge {:system :eia
            :region (if (string/starts-with? phosphor-or-yoke "B") :japan :usa)
            :family family
            :size (->Size (str->int size) :in (if (= viewable? "") :face :viewable))}
           (if (string/starts-with? phosphor-or-yoke "TC")
             {:bonded-yoke phosphor-or-yoke}
             {:phosphor (phos/normalize phosphor-or-yoke)})
           (when (not (= "" revision)) {:revision revision}))))


;; ([A-Z]*) ;; Before size
(def ^:private eiaj-re #"^([0-9]{3})([A-Z0-9]+)[BGD]([0-9]+)-?([A-Z0-9]*)")

(defn eiaj [^String text]
  (if-let [[_ size family phosphor yoke] (re-find eiaj-re text)]
    (merge {:system :eiaj
            :region :japan
            :family family
            :phosphor (phos/normalize phosphor)
            :size (->Size (str->int size) :mm :face)}
           (when (not (= "" yoke)) {:yoke yoke}))))



(def pro-electron-applications
  {"A" "Direct view TV tube"
   "D" "Single trace oscilloscope tube"
   "E" "Multiple trace oscilloscope tube"
   "F" "Direct view radar tube"
   "L" "Display storage tube"
   "M" "Direct view tube for professional TV / monitor"
   "P" "Projection tube"
   "Q" "Flying spot scanner"})

(def ^:private pro-electron-re #"^([ADEFLMPQ])([0-9]+)-([0-9A-Z]{2,3})([ABDGKLRWXY])?")

(defn pro-electron [^String text]
  (if-let [[_ application size family phosphor] (re-find pro-electron-re text)]
    (merge {:system :pro-electron
            :region :europe
            :application (get pro-electron-applications application)
            :size (->Size (str->int size) :cm :face)
            :family family}
           (when phosphor {:phosphor (phos/normalize phosphor)}))))



(def ^:private china-re
  #"^([0-9]{2})SX([0-9]{1,3})Y([0-9]+)-?(DC[0-9]+)?")

(defn china [^String text]
  (if-let [[_ size family phosphor yoke] (re-find china-re text)]
    (merge {:system :chinese
            :region :china
            :size (->Size (str->int size) :cm :face)
            :phosphor (str->int phosphor)}
           (when yoke {:yoke yoke}))))



(def ^:private tesla-ecimex-re #"^([0-9]{2})1QQ([0-9]+)")

(defn tesla-ecimex [^String text]
  (if-let [[_ size phosphor] (re-find tesla-ecimex-re text)]
    {:system :tesla-ecimex
     :region :czech-republic
     :size (->Size (str->int size) :cm :face)
     :phosphor (str->int phosphor)}))



(def known-unknown-1-re #"^(E)[0-9]{4}")
(def known-unknown-2-re #"^(CE|SD|SF|CT|P)[0-9]{2,3}")
(def known-unknown-3-re #"^([0-9]{2})?ST[0-9]{4}")
(def known-unknown-4-re #"^P[0-9]{2}[A-Z]{3}[0-9]{2}[A-Z]{3}")
(def known-unknown-5-re #"^[0-9]{2}[A-Z]{6}")
(def known-unknown-6-re #"^[0-9]{4}$")
(def known-unknown-7-re #"^[CR][0-9]{4}[BP](0-9)")
(def known-unknown-8-re #"^[0-9]ST[0-9]{4}")

;; "P14LKJ01RFA"
;; "P16LGX01RJA"
;; "P16LKK03RFA"
;; "P16LLN07RJA"
;; "12ABYWDN"

(defn known-unknown [^String text]
  (or (re-find known-unknown-1-re text)
      (re-find known-unknown-2-re text)
      (re-find known-unknown-3-re text)
      (re-find known-unknown-4-re text)
      (re-find known-unknown-5-re text)
      (re-find known-unknown-6-re text)
      (re-find known-unknown-7-re text)
      (re-find known-unknown-8-re text)
      ))



(def generic-phosphor-re #"[BP]([0-9]{1,2})")
(def generic-size-re #"^[A-Z]*([0-9]{1,3})")

(defn generic "Attempt to extract info from an unknown system."
  [^String text]
  (merge
   {:system :unknown}
   (if-let [[_ phosphor] (re-find generic-phosphor-re text)]
     {:phosphor (str->int phosphor)})
   (if-let [[_ size] (re-find generic-size-re text)]
     (let [size (str->int size)
           size (if (and (> size 100) (> (rem size 10) 0))
                  (int (/ size 10.0))
                  size)]
       {:size (->Size size (cond
                             (<= size 30) :in
                             (>= size 300) :mm
                             true :cm) :face)}))))

(defn guess-system [text]
  (condp re-find text
    #"P(4|22)\b" eia
    #"B(4|22)\b" eiaj
    #"\bA(34|48|63)" wtds
    nil))


(defn prefix "Iteratively strip prefix chars, attempting f."
  [^String text f]
  (loop [n 0
         len (count text)]
    (or (f (subs text n))
        (and (< n (- len 4)) (recur (+ 1 n) len)))))

(def ^:private parsers [wtds eia eiaj pro-electron china tesla-ecimex])

(defn- brute-force [^String text]
  (loop [[f & rem] parsers]
    (or (and f (f text)) (and rem (recur rem)))))

(defn parse [^String text]
  (let [text (cleanup text)
        parsed? (if-let [sysp (guess-system text)]
                  (prefix text sysp))]
    (or parsed? (brute-force text)
        (generic text))))
