;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2017, 2021, 2022 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.tube.compat
  (:require [clojure.core.match :refer [match]]
            [clojure.set :as set]
            [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [clojure.string :as string]
            [taoensso.timbre :as timbre
             #?(:clj :refer
                :cljs :refer-macros)
             [infof]]
            [tubular.math :as math]
            [tubular.phosphor :as phos]
            [tubular.spec.db :as sdb]
            [tubular.spec.tube :as stube]
            [tubular.spec.tube.compat :as tc]
            [tubular.tube.parser :as parser]
            [tubular.tube.size :as sizes]))

(defn pinout-compat? "Are two tubes electrically compatible?"
  [[_ {bka :bk}] [_ {bkb :bk}]]
  (cond
    (not (and bka bkb)) ::tc/unknown
    (= bka bkb)         ::tc/compat
    :default            ::tc/incompat))

(defn size-compat? "Are tube designators A and B the same size?"
  [[{{atype :type, :as a} :size} _] [{{btype :type, :as b} :size} _]]
  (let [c? (sizes/compat? a b)]
    (cond
      (and c? (= atype btype)) ::tc/compat
      c?                       ::tc/maybe
      :default                 ::tc/incompat)))

(defn describe "Return a human-readable description of a compatibility report."
  [report]
  (match report
         {::tc/size   ::tc/incompat} "Wrong size"
         {::tc/pinout ::tc/incompat} "Wrong pinout"

         {::tc/size ::tc/compat, ::tc/pinout ::tc/compat}
         "Size and pinout compatible"

         {::tc/pinout ::tc/compat, ::tc/size ::tc/maybe}
         "Pinout compatible, but size may differ"

         :else "Unknown"))

(s/fdef describe
  :args (s/cat :report ::tc/report)
  :ret string?)

(s/fdef size-compat?
  :ret ::tc/compat?)

(def checks "Tests to make to determine tube compatibility."
  [::tc/size   size-compat?
   ::tc/pinout pinout-compat?])



(defn compat?
  "Return a detailed compatibility breakdown for two CRTs."
  [a b]
  (->> (for [[type test] (partition 2 checks)] [type (test a b)])
       (into {})))

(s/fdef compat?
  :args (s/cat :a ::tc/tube-data, :b ::tc/tube-data)
  :ret ::tc/report)

(defn simple-compat?
  "Return a summary of a compatibility report.

  Returns the least compatible output of all tests in the report."
  [compat] (apply tc/worst (vals compat)))

(s/fdef simple-compat?
  :args ::tc/report
  :ret ::tc/compat?)

(defn quality
  "Return the compatibilty qusality of a report.

  Quality is determined by summing the weights of the report
  compatibility values.  Lower is more compatible."
  [r]
  (transduce
   (map #(get tc/weights %))
   +
   (vals r)))

(s/fdef quality
  :args (s/cat :report ::tc/report)
  :ret int?)

#_(defn compat-quality [[_ r]]
  (quality r))

(defn compat>
  "Rank "
  [[a areport] [b breport]]
  (let [rc (- (quality areport) (quality breport))]
    (if-not (zero? rc) rc
            (compare a b))))

(defn maybe-compat?
  "Does a compat report indicate possible compatibility?

  Returns true if a report only contains explicitly or maybe
  compatible.  Returns false if explicitly incompatible or unknown."
  [report]
  (empty? (set/intersection #{::tc/incompat ::tc/unknown} (into #{} (vals report)))))

(s/fdef maybe-compat?
  :args (s/cat :report ::tc/report)
  :ret boolean?)

(defn all-compat
  "Return all tubes in TUBES which may be compatible with PDESIG/TUBE."
  [tubes pdesig tube]
  (let [compat? (partial compat? [pdesig tube])]
    (sort-by identity compat>
             (sequence
              (comp
               (map
                ;; Applied across all tubes
                (fn check-compat [[match-desig match-tube]]
                  ;; Compat struct
                  (let [{:keys [:system :size] :as p} (parser/parse match-desig)]
                    (when (and size (not (= :unknown system)))
                      [match-desig (compat? [p match-tube])]))))
               ;; Remove anything that failed the map — no size or
               ;; unknown designation system.
               (remove nil?)
               ;; Remove anything either explicitly incompatible or
               ;; unknown compatibility.
               (filter (fn [[_ r]] (maybe-compat? r))))
              tubes))))
(s/fdef all-compat
  :args (s/cat :tubes ::sdb/tubes,
               :pdesig map?
               :tube ::stube/tube)
  :ret (s/coll-of (s/tuple ::stube/desig ::tc/report)))

(defn- merge-compat*
  "Helper function for merge-compat."
  [acc tubes]
  (let [na (for [existing acc]
             (if-not (empty? (set/intersection existing tubes))
               (set/union existing tubes)
               existing))]
    (cond
      (empty? na) [tubes]
      (= acc na) (conj acc tubes)
      :default na)))

(s/fdef merge-compat*
  :args (s/cat :acc ::tc/compats, :tubes ::tc/eqiuvalent)
  :ret ::tc/compats)

(defn merge-compat
  "Utility function to merge compatible tubes.

  Takes a vector of sets of of compatible designations, and combines
  them into non-overlapping sets.  For example, if given:

  [#{:a :b} #{:b :c} #{:d :e}]

  Will return:

  [#{:a :b :c} #{:d :e}]"

  [compats]
  (loop [acc []
         [c & rest] compats]
    (if (nil? c) (into [] acc)
        (-> (merge-compat* acc c)
            (recur rest)))))

(s/fdef merge-compat
  :args (s/cat :tubes ::tc/compats)
  :ret ::tc/compats)

(defn used-in? [{:keys [:tubes :verified-tubes] :as display} desig]
  (or (contains? tubes desig)
      (contains? verified-tubes desig)))

(defn- used-in-key [[_ {:keys [:application :manufacturer :model]}]]
  [application manufacturer model])

(defn used-in "Return displays TUBE is used in."
  [displays desig]
  (sort-by used-in-key
           (into {}
                 (filter (fn [[slug disp]] (used-in? disp desig)))
                 displays)))
