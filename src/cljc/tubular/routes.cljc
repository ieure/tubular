;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.routes
  (:require [bidi.bidi :as bidi]
            [clojure.spec.alpha :as s]
            [re-frame.core :as re-frame]
            [taoensso.timbre :as timbre
             #?(:clj :refer
                :cljs :refer-macros)
             [debugf]]
            [tubular.config :as config]
            [tubular.events :as events]
            [tubular.spec.db :as db]
            [tubular.spec.display :as sdisp]
            [tubular.spec.tube :as stube]))

;; TODO: Find a cleaner way of doing this.
;;
;; Bidi likes to do the wrong thing when you have a single handler
;; which optionally takes more info ex. /tube/foo
;; vs. /tube/foo/xref/bar, and those details leak all over the place,
;; like tube.subs/active?.
;;
;; Maybe conforming to the spec panel in url-for at least keeps the
;; damage localized?
(def routes
  [config/base
   {""                           ::db/home
    ;; ["display/"   ::sdisp/slug]  ::db/display
    ["search/"    ::db/search]   ::db/home-search
    ["tube/"      ::stube/desig] ::db/tube
    ["xref/tube/" ::stube/desig] ::db/tube-xref}])

(defn parse [url]
  (bidi/match-route routes url))

(defn unparse [{:keys [:handler :route-params]}]
  (bidi/path-for* routes handler route-params))
(s/fdef unparse :ret (s/nilable string?))

(defn url-for [handler & args]
  (apply bidi/path-for routes handler args))
(s/fdef url-for :ret (s/nilable string?))

(defn- dispatch-panel [{:keys [handler route-params] :as route}]
  (if route-params
    [handler route-params]
    [handler]))

(s/fdef dispatch-panel
  :ret ::db/panel)

(defn dispatch [route]
  (let [e `[::events/set-panel! ~@(dispatch-panel route)]]
    (debugf "routes/dispatch %s" e)
    (re-frame/dispatch e)))
