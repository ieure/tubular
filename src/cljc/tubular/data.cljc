;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.data
  (:require #?(:clj [clojure.java.io :as io]
               :cljs [shadow.resource :as rc])
            [clojure.tools.reader.edn :as edn]
            [cognitect.transit :as transit]))

(defn load! []
  #?(:cljs
     (-> (transit/reader :json)
         (transit/read (rc/inline "/data.transit")))

     :clj
     (-> (io/input-stream (io/resource "data.transit"))
         (transit/reader :json)
         transit/read)))
