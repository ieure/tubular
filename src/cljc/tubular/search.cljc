;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2013-2022 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.search
  (:require
   [clojure.spec.alpha :as s]
   [clojure.string :as string]
   [tubular.data :as data]
   [tubular.spec.search :as ss]
   [tubular.spec.db :as db]
   [tubular.tube.parser :refer [parse]]))

 ;; Matchers

(defn regex? [text]
  "Returns non-nil if TEXT looks like it might be a regular expression."
  (some #(string/index-of text %) #{\* \$ \^ \[ \]}))

(defn- search-type [text]
  "Return the type of search represented by TEXT."
  (cond
    (regex? text) :regex
    :default :string))

(defmulti matcher-for search-type)

(defmethod matcher-for :regex [search]
  {:pre [(string? search)]}
  #(re-find (re-pattern (str "(?i)" (string/trim search))) %))

(defmethod matcher-for :string [search]
  {:pre [(string? search)]}
  (let [search (string/trim (string/capitalize search))]
    #(string/starts-with? (string/capitalize %) search)))

(defn matching-tube-xf "Return transduce xf for searching tubes."
  [^String text]
  (let [m (matcher-for text)]
    (filter (fn [[id _]] (m id)))))

(defn matching-tubes*
  "Return tubes matching TEXT.

  Provided xfs will be composed into the transduce."
  [tubes text & xfs]
   (when-not (string/blank? text)
     (sequence (apply comp (matching-tube-xf text) xfs) tubes)))

(defn matching-tubes
  "Return tubes matching TEXT."
  [tubes text]
  (matching-tubes* tubes text
                   (map (fn [[desig _]] [::ss/tube desig]))))

(defn- display-matches? "Return non-NIL if DISP matches query TEXT."
  [text [slug {:keys [tubes manufacturer model notes]} :as disp]]
  (or (string/includes? (string/upper-case model) text)
      (string/includes? (string/upper-case manufacturer) text)
      (string/includes? (string/upper-case (str notes)) text)
      (some #(string/includes? (string/upper-case %) text) tubes)))

(defn matching-displays "Return displays matching TEXT."
  [displays text]
  []
  #_(when-not (string/blank? text)
     (sequence
      (comp (filter #(display-matches? (string/upper-case text) %))
            (map (fn [[slug _]] [::ss/display slug])))
      displays)))

(defn everything "Return tubes & displays matching TEXT."
  [text {:keys [::db/tubes ::db/displays] :as data}]
  {:pre [(s/assert string? text)]
   :post [(s/assert ::ss/results %)]}
  (lazy-cat (matching-tubes tubes text)
            (matching-displays displays text)))
