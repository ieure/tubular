;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.slug
  (:require [clojure.string :as string]
            [taoensso.timbre :as timbre
             #?(:clj :refer
                :cljs :refer-macros)
             [errorf warnf]]
            [tubular.display :as display]))

(defn slug [mon]
  (try
    (-> (display/describe mon :manufacturer :size :model)
        (string/lower-case)
        (string/replace #"[^a-z0-9]+" " ")
        (string/trim)
        (string/replace #" " "-"))
    (catch #?(:clj Exception
              :cljs js/Object) e
      (errorf "Failed to slugify %s due to %s" mon e)
      "fixme-broken-slug")))
