;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.voltage
  (:require [clojure.spec.alpha :as s]
            [tubular.spec.common :as com]))

(s/def ::cathode ::com/v-or-range)
(s/def ::anode ::com/kv-or-range)
(s/def ::filament ::com/volts)
(s/def ::g2 ::com/volts)
(s/def ::focus ::com/v-or-range)

(s/def ::voltage (s/keys :opt-un [::cathode ::anode ::filament ::g2 ::focus]))
