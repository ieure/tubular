;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure
;; Author: Ian Eure <ian.eure@gmail.com>
;;
(ns tubular.spec.tube.size
  (:require [clojure.spec.alpha :as s]
            [tubular.spec.common :as com]))

(s/def ::type (com/enum ::viewable ::face))

(s/def ::unit (com/enum ::mm ::cm ::in))

(s/def ::n int?)

(s/keys ::size (s/keys ::req [::n ::unit ::type]))
