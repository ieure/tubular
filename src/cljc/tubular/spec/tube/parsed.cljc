;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure
;; Author: Ian Eure <ian.eure@gmail.com>
;;
(ns tubular.spec.tube.parsed
  (:require [clojure.spec.alpha :as s]))
