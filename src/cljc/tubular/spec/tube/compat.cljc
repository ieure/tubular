;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.tube.compat
  (:require [clojure.spec.alpha :as s]
            [tubular.spec.common :as com]
            [tubular.spec.tube :as stube]))


(def compats
  {::compat "compatible"
   ::maybe "possibly compatible"
   ::incompat "incompatible"
   ::unknown "not enough data to determine compatibility"})

(s/def ::compat   (com/const ::compat))
(s/def ::maybe    (com/const ::maybe))
(s/def ::incompat (com/const ::incompat))
(s/def ::unknown  (com/const ::unknown))

(s/def ::compat?
  (s/or :compat ::compat
        :maybe ::maybe
        :incompat ::incompat
        :unknown ::unknown))

(s/def ::pinout ::compat?)
(s/def ::size ::compat?)

(s/def ::report (s/keys :req [::pinout ::size]))

(def weights "Compatibility weights.  Lower are more compatible."
  {::compat 0
   ::maybe 10
   ::unknown 50
   ::incompat 1000})

(defn sortf [c]
  (get weights c))

(defn worst "Return the least compatible of `cs'"
  [& cs]
  (->> cs
       (sort-by sortf)
       reverse
       first))

(def types
  {::pinout "pinout"
   ::size   "size"})

(defn describe [type compat]
  (str (get types type) " is " (get compats compat)))

;; TODO: Spec over tube parser
(s/def ::tube-data (s/tuple map? ::stube/tube))

;; A set of interchangeable tubes.
(s/def ::equivalent (s/coll-of ::stube/desig :kind set?))
;; A sequence of sets of interchangeable tubes.
(s/def ::compats (s/coll-of ::equivalent :kind vector?))
