;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.db
  (:require [clojure.spec.alpha :as s]
            [tubular.config :as config]
            [tubular.spec.common :as com]
            [tubular.spec.display :as sdisp]
            [tubular.spec.tube :as stube]
            [tubular.spec.tube.compat :as scompat]))

 ;; Loading

(s/def ::loading (s/tuple (com/const ::loading)))

;; Home

(s/def ::search ::com/non-empty-string)

(s/def ::home        (s/tuple (com/const ::home)))
(s/def ::home-search (s/tuple (com/const ::home) (s/keys :req [::search])))

;; Tube page

(s/def ::tube (s/tuple (com/const ::tube) (s/keys :req [::stube/desig])))

;; Tube cross-reference page

(s/def ::tube-xref (s/tuple (com/const ::tube-xref) (s/keys :req [::stube/desig])))

;; Display page

;; Displays can have tubes cross-referenced as well.
(s/def ::display (s/tuple (com/const ::display) (s/keys :req [::sdisp/slug])))

 ;; DB

;; Titles are a breadcrumb of string components.
(s/def ::title (s/coll-of ::com/non-empty-string :type vector?))

(s/def ::tubes    (s/map-of ::stube/desig ::stube/tube))
(s/def ::displays (s/map-of ::sdisp/slug  ::sdisp/display))
(s/def ::equiv    ::scompat/compats)

;; All data is stored in the DB.
(s/def ::data (s/keys :req [::tubes ::equiv]
                      :opt [::displays]))

(s/def ::panel (s/or :loading      ::loading
                     :home         ::home
                     :home-search  ::home-search
                     :tube         ::tube
                     :tube-xref    ::tube-xref
                     :display      ::display))

(s/def ::db (s/keys :req [::name ::panel]
                    :opt [::data ::title]))

;; DB zero state.  Data is loaded by the ::events/initialize-db.
(def default-db
  {::name config/app-name
   ::panel [::loading]})
