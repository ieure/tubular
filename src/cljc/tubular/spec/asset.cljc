;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.asset
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [tubular.spec.common :as com]))

(s/def ::url (s/tuple (com/const :url) ::com/url))

(s/def ::bk-adapter-number
  (com/enum 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22
            23 24 25 26 27 28 29 30 31 32 33 35 36 37 38 39 40 41 42
            43 44 45 70 71))

;; The JEDEC publication number.
(s/def ::jedec-release (s/and string? #(re-matches #"^[0-9]+[A-Z]?$" %)))

(s/def ::jedec (s/tuple (com/const :jedec) ::jedec-release))

(s/def ::crt (s/tuple (com/const :crt) ::com/non-empty-string))

(s/def ::bkcr (s/tuple (com/const :bkcr) ::bk-adapter-number))

(s/def ::datasheet
  (s/or :jedec ::jedec
        :crt ::crt
        :bkcr ::bkcr))
