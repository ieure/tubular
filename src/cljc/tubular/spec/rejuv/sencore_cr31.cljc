;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.rejuv.sencore-cr31
  (:require [clojure.spec.alpha :as s]
            [tubular.spec.common :as com]))

(defonce bias-voltage
  {:a -20
   :b -35
   :c -50
   :d -70
   :e -100})

(s/def ::socket (s/or :num int?
                      :s #(re-matches #"^X[0-9]{1,3}$" %)))
(s/def ::bias (com/enum :a :b :c :d :e))

(s/def ::cr31 (s/keys :opt-un [::socket ::bias]))
