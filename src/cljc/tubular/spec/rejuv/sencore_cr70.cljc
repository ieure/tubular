;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.rejuv.sencore-cr70
  (:require [clojure.spec.alpha :as s]
            [tubular.spec.common :as com]))

(s/def ::socket (com/enum  "2" "3" "4" "5/10" "6" "7" "8" #_"8/UA" "9" "13" #_"13/UA" "UA"))
(s/def ::bias #(re-matches #"^[0-9]{2}V$" %))
(s/def ::type (com/enum "Video" "Proj" "Scope"))

(s/def ::cr70 (s/keys :opt-un [::socket ::bias ::type]))
