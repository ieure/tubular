;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.rejuv.bk
  (:require [clojure.spec.alpha :as s]
            [tubular.spec.common :as com]))

(s/def ::filament float?)
(s/def ::adapter int?)
(s/def ::g1 int?)

(s/def ::option (com/enum :brg :grb))

(def options
  {:brg "Red and blue pins are swapped"
   :grb "Red and green pins are swapped"})

(s/def ::normal (s/keys :req-un [::filament ::adapter ::g1]
                        :opt-un [::option]))

(s/def ::special
  (s/keys :req-un [::filament ::adapter ::g1-467&90 ::g1-470&80]
          :opt-un [::option]))

(s/def ::bk (s/or :normal ::normal
                  :special ::special))

;; CRTs marked with (@), set G1 to 100 V
;; CRTs marked with (*), set G1 to 70 V
;; CRTs marked with (**), set G1 to 30 V
;; CRTs marked with (@@), 467 & 490 set G1 to 30V, 470 & 480 set G1 to 20V
