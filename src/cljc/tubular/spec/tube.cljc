;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.tube
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [tubular.spec.asset :as asset]
            [tubular.spec.common :as com]
            [tubular.spec.pinout :as pinout]
            [tubular.spec.rejuv.bk :as bk]
            [tubular.spec.rejuv.sencore-cr31 :as cr31]
            [tubular.spec.rejuv.sencore-cr70 :as cr70]
            [tubular.spec.rejuv.sencore-cr7000 :as cr7000]
            [tubular.spec.voltage :as voltage]))

(s/def ::datasheets (s/coll-of ::asset/datasheet :kind vector))

;; Most tubes aren't direct heat, so only allow specifying when they
;; are.
(s/def ::direct-heat #(= true %))

(s/def ::rejuv (s/keys :opt-un [::bk/bk ::cr31/cr31 ::cr70/cr70 ::cr7000/cr7000]))

(s/def ::tube
  (s/keys :opt-un [::rejuv
                   ::pinout/pinout
                   ::direct-heat
                   ::voltage/voltage
                   ::datasheets]))

;; A tube designator
(s/def ::desig ::com/non-empty-string)

;; A cross reference is also a designator.
(s/def ::xref ::desig)
