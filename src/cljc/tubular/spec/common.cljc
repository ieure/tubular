;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.common
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]))

(s/def ::non-empty-string (s/and string? (complement string/blank?)))

(s/def ::url ::non-empty-string)

(s/def ::pin int?)

(s/def ::volts number?)
(s/def ::kilovolts ::volts)

(s/def ::v-or-range
  (s/or :single ::volts
        :range string?))

(s/def ::kv-or-range
  (s/or :single ::kilovolts
        :range string?))

(s/def ::i float?)                      ; Inductance, millihenries
(s/def ::r float?)                      ; Resistance, ohms

(s/def ::slug ::non-empty-string)

(defn const [val]
  #(= % val))

(defn enum [& vals]
  #(contains? (into #{} vals) %))
