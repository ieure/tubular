;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.yoke
  (:require [clojure.spec.alpha :as s]
            [tubular.spec.common :as com]))

(s/def ::axis-measurement
  (s/or
   :resistance (s/keys :req-un [::com/r] :opt [::com/i])
   :inductance (s/keys :req-un [::com/i] :opt [::com/r])))

(s/def ::h ::axis-measurement)
(s/def ::v ::axis-measurement)

(s/def ::id (s/coll-of ::com/non-empty-string :kind vector?))

(s/def ::yoke
  (s/keys :opt-un [::id ::h ::v]))
