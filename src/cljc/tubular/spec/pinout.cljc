;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.pinout
  (:require [clojure.spec.alpha :as s]
            [tubular.spec.common :as com]))

;; TODO: "X" values are likely bunk, investigate further.
(s/def ::f1 (s/or :pin ::com/pin :x (com/const "X")))                  ; Filament 1
(s/def ::f2 (s/or :pin ::com/pin :x (com/const "X")))                  ; Filament 2

;; Cathode.  Always distinct per gun.
(s/def ::k ::com/pin)

;; Control grid.  Usually, but not always, tied together on color tubes.
(s/def ::g1 ::com/pin)
;; Screen grid.  Usually, but not always, tied together on color tubes.
(s/def ::g2 ::com/pin)

(s/def ::gun (s/keys :req-un [::k] :opt-un [::g1 ::g2]))

(s/def ::r ::gun)
(s/def ::g ::gun)
(s/def ::b ::gun)

(s/def ::rgb (s/keys :req-un [::f1 ::f2 ::r ::g ::b]))

(s/def ::bw (s/keys :req-un [::f1 ::f2 ::r]))

(s/def ::incomplete (s/keys :opt-un [::f1 ::f2 ::r ::g ::b]))

(s/def ::pinout
  (s/or :rgb ::rgb
        :bw ::bw
        :incomplete ::incomplete))
