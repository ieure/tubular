;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.display
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [tubular.spec.common :as com]
            [tubular.spec.yoke :as yoke]))

(s/def ::application (com/enum :game :tv :computer))

(defonce applications {:tv "TV"
                       :computer "PC Monitor"
                       :game "Arcade Monitor"})

(s/def ::scan (com/enum :vector :raster))

(s/def ::docs (s/map-of string? ::com/url))

(s/def ::manufacturer ::com/non-empty-string)
(s/def ::model ::com/non-empty-string)

(s/def ::sync (s/coll-of int? :kind vector?))

(s/def ::photos (s/coll-of ::com/url :kind vector?))

(s/def ::input (com/enum
                :rgbi                   ; TTL video, ex. IBM CGA
                :rgbs                   ; Analog RGB
                :composite              ; B&W video or composite color video
                :yc                     ; ex. S-Video
                ))

(s/def ::inputs (s/coll-of ::input :kind set?))

(s/def ::display
  (s/keys :req-un [::manufacturer ::model,
                   ::application
                   ::tubes]
          :opt-un [::docs ::notes ::yoke/yoke
                   ::inputs
                   ::sync]))

;; Displays have a slug, which is the key they're looked up by in
;; ::data.
(s/def ::slug ::com/non-empty-string)
