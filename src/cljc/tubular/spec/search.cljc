;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2013-2022 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.search
  (:require [clojure.spec.alpha :as s]
            [tubular.spec.tube :as stube]
            [tubular.spec.display :as sdisp]
            [tubular.spec.common :as com]))

(s/def ::tube    (s/tuple (com/const ::tube)    ::stube/desig))
(s/def ::display (s/tuple (com/const ::display) ::sdisp/slug))

(s/def ::result
  (s/or :tube ::tube
        :display ::display))

(s/def ::results (s/coll-of ::result))
