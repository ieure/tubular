;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.asset
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [tubular.config :as config]
            [tubular.spec.asset :as sasset]
            [tubular.spec.common :as com]))

(defn datasheet-type [ds]
  (let [dt (s/conform ::sasset/datasheet ds)]
    (if (= ::s/invalid dt)
      dt
      (first dt))))

(defn datasheet-type-text [type]
  (condp = type
    :jedec "JEDEC"))

(defmulti datasheet
  "Return the URL for a datasheet."
  datasheet-type)

(defmethod datasheet ::s/invalid [_] nil)

(defmethod datasheet :jedec [[_ release]]
  (str config/asset-base "/crt/jedec/" release ".pdf"))

(defmethod datasheet :crt [[_ crt]]
  (str config/asset-base "/crt/generic/" crt ".pdf"))

(defmethod datasheet :bkcr
  [[_ n]]
  (str config/asset-base "/adapters/bk/CR-" n ".pdf"))

(s/fdef datasheet
  :args ::sasset/datasheet
  :ret ::com/url)



(defmulti link-text
  "Return the link text for a datasheet."
  datasheet-type)

(defmethod link-text ::s/invalid [& _] "")

(defmethod link-text :jedec [[_ release] verbose]
  (str
   (if verbose "JEDEC " "")
   "Release #" release))

(defmethod link-text :bkcr [[_ n] verbose]
  (str
   (if verbose "BK Precision " "")
   "CR-" n))
