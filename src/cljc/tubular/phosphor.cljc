;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © Copyright 2012-2021 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.phosphor
  (:require [clojure.string :as string]))

;; Taken from http://www.bunkerofdoom.com/tubes/crt/crt_phosphor_research.pdf
(defonce phosphors
  {:eia {:p1 {:desc "Green to yellowish-green"}
         :p2 {:desc "Blue-green"}
         :p3 {:desc "Yellow to yellow-green"}
         :p4 {:desc "White"}
         :p5 {:desc "Blue"}
         :p6 {:desc "White"}
         :p7 {:desc "Blue-white"}
         :p8 {:desc "Blue-white"}
         :p10 {:desc "Dark magenta"}
         :p11 {:desc "Blue to purplish-blue"}
         :p12 {:desc "Orange"}
         :p13 {:desc "Light red to reddish-orange"}
         :p14 {:desc "Violet to purplish-blue to blue"}
         :p15 {:desc "Blue-green ultraviolet to violet to ultraviolet"}
         :p17 {:desc "Greenish-yellow"}
         :p18 {:desc "White"}
         :p19 {:desc "Orange"}
         :p20 {:desc "Yellowish-green"}
         :p21 {:desc "Yellow"}
         :p22 {:desc "Red/green/blue"}
         :p22r {:desc "Red"}
         :p22g {:desc "Green"}
         :p22b {:desc "Blue"}
         :p23 {:desc "White"}
         :p24 {:desc "Blue-green or white"}
         :p25 {:desc "Orange"}
         :p26 {:desc "Yellow-orange to orange"}
         :p27 {:desc "Orange-red"}
         :p28 {:desc "Yellowish-green"}
         :p29 {:desc "P2/P25 striped"}
         :p31 {:desc "Green to yellowish-green"}
         :p32 {:desc "Blue green or purple-blue"}
         :p33 {:desc "Orange"}
         :p34 {:desc "Blue-green"}
         :p35 {:desc "Blue-white"}
         :p36 {:desc "Yellow-green"}
         :p37 {:desc "Blue"}
         :p38 {:desc "Orange"}
         :p39 {:desc "Green"}
         :p40 {:desc "White"}
         :p41 {:desc "Blue"}
         :p43 {:desc "Ultraviolet"}
         :p45 {:desc "Red"}
         :p46 {:desc "Green"}
         :p47 {:desc "Blue"}
         :p48 {:desc "Blue-green"}
         :p53 {:desc "Yellow-green"}
         :p55 {:desc "Blue"}
         :p56 {:desc "Red"}
         }
   :eu {:gj {:desc "Green to yellowish-green"}
        :gk {:desc "Green to yellowish-green"}
        :ww {:desc "White"}
        :bj {:desc "Blue"}
        :be {:desc "Blue"}
        :gg {:desc "Blue-green ultraviolet"}
        :lf {:desc "Yellow"}
        :ge {:desc "Green"}
        :lj {:desc "Orange"}
        :lc {:desc "Yellow-orange"}
        :ke {:desc "Yellow"}
        :gh {:desc "Yellowish-greeen"}
        :ld {:desc "Orange"}
        :lk {:desc "Orange"}
        :gr {:desc "Green"}
        :ga {:desc "White"}
        :gy {:desc "Yellow-green"}
        :wb {:desc "White"}
        :red-enh {:desc "Red"}
        :kg {:desc "Green"}
        :bh {:desc "Blue"}
        :kh {:desc "Blue-green"}
        :kj {:desc "Yellow-green"}
        :bm {:desc "Blue"}
        :rf {:desc "Red"}}

   :pro-electron {:a {:desc "reddish purple, purple, blueish purple"}
                  :b {:desc "purplish blue, blue, greenish blue"}
                  :d {:desc "blue-green"}
                  :g {:desc "blueish green, green, yellowish green"}
                  :k {:desc "yellow-green"}
                  :l {:desc "orange, orange-pink"}
                  :r {:desc "reddish orange, red, pink, purplish pink, purplish red, red-purple"}
                  :w {:desc "standard white (black and white tubes)"}
                  :x {:desc "red, green, blue (color picture tubes)"}
                  :y {:desc "greenish yellow, yellow, yellowish orange"}}

   :wtds {:x {:desc "Color"}
          :w {:desc "B&W"}}
   })

(defn numeric? [text]
  (re-matches #"^[0-9]+$" (str text)))

(defn normalize [text]
  (cond
    (and (keyword text) (string/starts-with? (str text) ":b"))
    (recur (-> (str text) (subs 1)))

    (keyword? text) text

    (numeric? text) (keyword (str "p" text))

    (and (string? text) (string/starts-with? (string/lower-case text) "b"))
    (keyword (str "p" (subs text 1)))

    true (keyword (string/lower-case (str text)))))

(defn describe [type phosphor]
  (let [np (normalize phosphor)
        readable (-> np str (subs 1) (string/upper-case))]
    (string/join ", "
                 (filter identity [readable
                  (:desc (get-in phosphors [(if (= type :eiaj) :eia type) np]))]))))

(defonce color-phosphors
  #{:p22 :x})

(defn color? [phosphor]
  (contains? color-phosphors (normalize phosphor)))
