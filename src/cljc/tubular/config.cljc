;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.config)

(def debug? "Non-nil if Tubular is in debug mode."
  ^boolean #?(:cljs goog.DEBUG
              :clj (boolean (System/getenv "DEBUG"))))

(defonce app-name "Tubular")

(defonce title-sep " — ")

(defonce max-results 100)

(def base "Base path where the application is hosted."
  "/")

(def asset-base "Base path where assets are hosted."
  "https://assets.tubular.atomized.org")
