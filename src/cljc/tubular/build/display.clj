;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.build.display
  "CLJ display builder.

  This runs at build time, and outputs the data file used by the CLJS
  side of Tubular."
  (:require [clojure.java.io :as io]
            [clojure.set :as set]
            [tubular.build.common :as common]
            [tubular.slug :refer [slug]]))

 ;; Displays

(defn- display? [^java.io.File f]
  "Returns non-nil if f is a display EDN file."

  (and (.isFile f)
       (re-matches #".*\.edn$" (.getPath f))))

(defn files
  "Return data files for displays."

  ([]         (files "data/display"))
  ([data-dir]
   (->> (io/file data-dir)
        file-seq
        (filter display?))))

(defn- load! [^java.io.File f]
  (let [disp (common/load* f)]
    {(slug disp) disp}))

(defn build
  "Return a map of {SLUG DISPLAY ...} for all displays."
  ([]      (build (files)))
  ([files] (into {} (pmap load! files))))

(defn equivalent [displays]
  (sequence
   (comp
    (map #(select-keys % [:tubes :verified-tubes]))
    (map vals)
    (mapcat set/union)
    (remove #(= 1 (count %))))
   (vals displays)))
