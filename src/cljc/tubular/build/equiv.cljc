;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.build.equiv
  (:require [clojure.java.io :as io]
            [tubular.build.common :as com]
            [tubular.slug :refer [slug]]
            [tubular.tube.compat :as compat]))

(defn files
  "Return data files for equivalent tubes"

  ([]         (files "data/equiv"))
  ([data-dir] (com/files data-dir com/edn?)))

(defn build []
  (compat/merge-compat (mapcat com/load* (files))))
