;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.build.data
  "CLJ display builder.

  This runs at build time, and outputs the data file used by the CLJS
  side of Tubular."
  (:require [clojure.java.io :as io]
            [tubular.build.common :as common]
            [tubular.build.display :as display]
            [tubular.build.equiv :as equiv]
            [tubular.build.tube :as tube]
            [tubular.tube.compat :as compat]))

(defn load! []
  (let [displays (future (display/build))
        tubes    (future (tube/build))
        equiv    (future (equiv/build))]
    {:tubular.spec.db/displays @displays
     :tubular.spec.db/tubes    @tubes
     :tubular.spec.db/equiv    (compat/merge-compat
                                (concat @equiv
                                        (display/equivalent @displays)))}))

(defn build []
  (println "Processing data...")
  (common/write! "resources/data.transit" (load!))
  (println "Done."))
