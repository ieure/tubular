;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.build.tube
  "CLJ tube builder.

  This runs at build time, and outputs the data file used by the CLJS
  side of Tubular."
  (:require [clojure.java.io :as io]
            [clojure.string :as string]
            [tubular.build.common :as common]))

(defn- tube? [^java.io.File f]
  "Returns non-nil if f is tube display EDN file."

  (and (.isFile f)
       (re-matches #".*\.edn$" (.getPath f))))

(defn files
  "Return data files for tubes."

  ([]         (files "data/tube"))
  ([data-dir]
   (->> (io/file data-dir)
        file-seq
        (filter tube?))))

(defn slug [text]
  "Turn TEXT into a URL slug."
  (string/replace text "/" "_"))

(defn deslug [slug]
  "Turn SLUG back into text."
  (string/replace slug "_" "/"))

(defn- load! [^java.io.File f]
  "Load a single file of tube data off disk and return it."
  (let [desig (-> f (.. toPath getFileName toString)
                  (string/replace #"\.edn$" ""))]
    {desig (common/load* f)}))

(defn build
  "Return a map of {DESIG TUBE ...} for all tubes."
  ([]      (build (files)))
  ([files] (reduce conj (pmap load! files))))

(defn write! "Write tube data"
  [tubes]
  (doseq [[tube data] tubes]
    (common/write-raw! (format "data/tube/%s.edn" (slug tube)) data)))
