;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2017, 2021, 2022 Ian Eure
;; Author: Ian Eure <ian.eure@gmail.com>
;;
(ns tubular.build.common
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :refer [pprint]]
            [cognitect.transit :as transit]
            [taoensso.timbre :as log])

  (:import [java.io File PushbackReader]))

(def ^:dynamic *verbose* nil)

(defn write-raw! [dest forms]
  (with-open [writer (io/writer dest)]
    (binding [*out* writer]
      (pprint forms))))

(defn write! [dest forms]
  (with-open [writer  (io/output-stream dest)]
    (transit/write (transit/writer writer :json) forms)))

(defn load* [^File edn-file]
  (when *verbose* (printf "Loading `%s'\n" edn-file))
  (try
    (with-open [mio (PushbackReader. (io/reader edn-file))]
      (edn/read mio))
    (catch Exception e
      (log/errorf e (format "Failed to load %s" (.getPath edn-file)))
      (throw e))))

(defn edn? [^java.io.File f]
  "Returns non-nil if f is an EDN file."

  (and (.isFile f)
       (re-matches #".*\.edn$" (.getPath f))))

(defn files
  [data-dir & [pred?]]
  (->> (io/file data-dir)
       file-seq
       (filter pred?)))
