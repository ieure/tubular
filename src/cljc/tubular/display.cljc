;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.display
  (:require [clojure.string :as string]
            [tubular.phosphor :as phosphor]
            [tubular.spec.display :as sdisp]
            [tubular.tube.parser :as tparse]
            [tubular.tube.size :as size]))

(def default-format [:manufacturer :model])
(def ^:dynamic *format* default-format )

(defn describe [{:keys [:application :manufacturer :model :tubes] :as disp} & format]
  (let [{:keys [:phosphor :size] :as parsed} (tparse/parse (first tubes))]
    (->> (for [f (or format *format*)]
           (condp = f
             :color        (if (phosphor/color? phosphor) "Color" "B&W")
             :application  (get sdisp/applications application)
             :size         (size/describe-inches size)
             :manufacturer manufacturer
             :model        model))
         (filter identity)
         (string/join " "))))
