;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.events
  (:require [clojure.set :as set]
            [clojure.spec.alpha :as s]
            [re-frame.core :as re-frame]
            [taoensso.timbre :as timbre
             #?(:clj :refer
                :cljs :refer-macros)
             [debugf infof]]
            [tubular.data :as data]
            [tubular.spec.db :as sdb]
            [tubular.title :refer [title]]
            [tubular.tube.compat :as compat]))

(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   (let [{:keys [::sdb/tubes ::sdb/displays ::sdb/equiv] :as data} (data/load!)]
     (infof "Loaded %d tubes, %d displays, %d equiv clusters"
            (count tubes) (count displays) (count equiv))
     (assoc sdb/default-db ::sdb/data data))))

(def set-title-interceptor
  (re-frame/->interceptor
   :id ::set-panel!
   :after (fn [{{[_ & panel] :event} :coeffects :as context}]
            (debugf "set-title-interceptor panel=%s" panel)
            #?(:cljs (set! (. js/document -title) (title (into [] panel))))
            context)))

(re-frame/reg-event-db
 ::set-panel!
 [set-title-interceptor]
 (fn [db [_event panel args]]
   ;; {:post [(s/assert ::sdb/db %)]}
   (let [p+a (if args [panel args] [panel])]
     (debugf "::set-panel! -> %s" p+a)
     (s/assert ::sdb/panel p+a)
     (assoc db ::sdb/panel p+a))))

#_(re-frame/reg-event-fx
 ::set-panel!
 (fn [_ [_ handler & args]]
   (debugf "::navigate handler=%s args=%s" handler args)
   {::navigate `[~handler ~@args]}))
