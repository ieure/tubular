;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2013-2022 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.math)

(defn round [n]
  #?(:clj (java.lang.Math/round n)
     :cljs (js/Math.round n)))

(defn log [n]
  #?(:clj (java.lang.Math/log n)
     :cljs (js/Math.log n)))
