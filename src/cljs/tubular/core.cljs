;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.core
  (:require [clojure.spec.alpha :as s]
            [re-frame.core :as re-frame]
            [reagent.dom :as rdom]
            [taoensso.timbre :as timbre :refer-macros [debugf]]
            [tubular.config :as config]
            [tubular.events :as events]
            [tubular.nav :as nav]
            [tubular.routes :as routes]
            [tubular.views :as views]))

(defn dev-setup "Configure Tubular for development."
  []
  (if config/debug?
    (do
      (debugf "Enabling spec asserts")
      (timbre/set-level! :debug)
      (s/check-asserts true))

    (do
      (timbre/set-level! :warn)
      (s/check-asserts true))))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [views/main-panel] root-el)))

(defn init []
  (debugf "init -> ::events/initialize-db")
  (re-frame/dispatch-sync [::events/initialize-db])

  (debugf "init -> dev-setup")
  (dev-setup)

  (debugf "init -> mount-root")
  (mount-root)

  (debugf "init -> nav/start!")
  (nav/start!))
