;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.nav
  "Support for HTML5 navigation."
  (:require [pushy.core :as pushy]
            [re-frame.core :as re-frame]
            [taoensso.timbre :as timbre :refer-macros [debugf]]
            [tubular.events :as events]
            [tubular.routes :as routes]))

(def history "History control."
  (pushy/pushy routes/dispatch routes/parse))

(defn start! "Begin handling navigation events."
  [] (pushy/start! history))

(defn navigate! "Navigate to a different view."
  [[handler & args]]
  (let [url (apply routes/url-for handler args)]
    (debugf "navigate! handler%s args=%s → %s" handler args url)
    (pushy/set-token! history url)))

(re-frame/reg-fx ::events/navigate navigate!)
