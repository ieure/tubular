;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.home.subs
  "Subscriptions for the home view."
  (:require [clojure.string :as string]
            [re-frame.core :as rf]
            [taoensso.timbre :as timbre :refer-macros [debugf infof]]
            [tubular.config :as config]
            [tubular.search :as search]
            [tubular.spec.db :as db]
            [tubular.subs :as subs]))

;; Extract the current search term from the db.
(rf/reg-sub
 ::search
 :<- [::subs/panel+args]
 (fn [[panel {:keys [::db/search] :as args}]]
   (if (= ::db/home panel)
     search
     (infof "::search (nothing; wrong panel)"))))

;; Compute and return matches for the current search.
;; Returns a vector [query-text all-displays matching-results].
(rf/reg-sub
 ::results
 (fn []
   [(rf/subscribe [::search])
    (rf/subscribe [::subs/data])])
 (fn [[q {:keys [::db/tubes ::db/displays] :as data}]]
   (let [res (->> (search/everything q data)
                  (if (string/blank? q) [])
                  (when q)
                  (take config/max-results))]
     [q displays res])))
