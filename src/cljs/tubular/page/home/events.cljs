;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.home.events
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [re-frame.core :as rf]
            [taoensso.timbre :as timbre :refer-macros [debugf infof]]
            [tubular.spec.db :as sdb]))

(rf/reg-event-db
 ::search!
 (fn [{[panel {q ::sdb/search}] ::sdb/panel :as db} [_ q]]
   {:post [(s/assert ::sdb/db %)]}
   (when (= ::sdb/home panel)
     (if (and (string? q) (not (string/blank? q)))
       (update-in db [::sdb/panel 1] assoc ::sdb/search q)
       (update-in db [::sdb/panel] (fn [p] (into [] (butlast p))))))))
