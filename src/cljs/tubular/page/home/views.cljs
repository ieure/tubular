;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.home.views
  "Rendere fns for the home (search) page."
  (:require [bulma-cljs.core :as b]
            [clojure.string :as string]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [taoensso.timbre :as timbre :refer-macros [tracef infof]]
            [tubular.page.elements :as e]
            [tubular.page.home.events :as events]
            [tubular.page.home.subs :as subs]
            [tubular.spec.db :as tdb]))

(defn splash
  "Render the splash screen.

  This is shown when the search box is empty, and illustrates some of
  the things Tubular can do, for people who haven't used it before."

  []
  ^{:key "canned-searches"}
    [:div {:id "splash" :class "mt-5 mb-4"}
      [:header {:class "has-text-centered mb-4"}
        [:div {:class "columns is-centered"}
          [:div {:class "column is-5"}
            [:img {:src "/images/tubular.svg", :alt "Tubular", :class "image logo mt-5"}]]]
        [:div {:class "columns"}
          [:div {:class "column"}
            [:p {:class "is-size-5 is-size-6-mobile"}
              "A searchable database of vintage displays and the CRTs that power them."]]]]

   [b/columns
    #_[b/column
       [:h3 {:class "is-size-4"} "Displays"]
       [:dl
        [:dt "Electrohome Raster"]
        [:dd
         [e/display-short-link "electrohome-19-g07-cbo"] ", "
         [e/display-short-link "electrohome-13-g07-fbo"]]

        [:dt "Electrohome Vector"]
        [:dd
         [e/display-short-link "electrohome-19-g05-802"] ", "
         [e/display-short-link "electrohome-14-g05-805"] ", "
         [e/display-short-link "electrohome-19-g08-003"]]

        [:dt "Wells-Gardner Raster"]
        [:dd
         [e/display-short-link "wells-gardner-25-k7000"] ", "
         [e/display-short-link "wells-gardner-19-k7000"] ", "
         [e/display-short-link "wells-gardner-13-k7000"]]

        [:dt "Wells-Gardner Vector"]
        [:dd
         [e/display-short-link "wells-gardner-19-v2000"] ", "
         [e/display-short-link "wells-gardner-14-v2000"] ", "
         [e/display-short-link "wells-gardner-19-k6100"]]]]

    [b/column
     [:h3 {:class "is-size-4 has-text-weight-medium"} "Tubes"]
     [:dl
      [:dd [e/tube-link "19VLUP22"] " — Used in Atari & Sega color vectors"]
      [:dd [e/tube-link "19VARP4"] " — Used in many B&W vectors & rasters"]
      [:dd [e/tube-link "19VMNP22"] " — Used in many color rasters"]
      [:dd [e/tube-link "19VJTP22"] " — Used in many color rasters"]]]]])

(defn- update-search
  "Propagate an update to the search box."
  [evt]
  (let [q (-> evt .-target .-value)]
    (rf/dispatch [::events/search! (string/trim q)])))

(defn render
  "Render the main view of the home page.

  This shows the search box, and either the splash screen or search
  results."

  []
  (let [[q displays results] @(rf/subscribe [::subs/results])
        ta (r/atom nil)]
    (tracef "render search=`%s' found %d results" q (count results))
    [:div {:class "mt-6"}
     [b/input
      {:class "is-size-4"
       :type "search"
       :name "s" :id "s"
       :placeholder "🔍 CRT"
       :auto-complete "off"
       :auto-focus true
       :on-change
       (fn [evt]
         (let [tid @ta]
           (when tid (js/clearTimeout tid))
         (reset! ta
                 (-> (fn [] (js/clearTimeout tid)
                       (update-search evt))
                     (js/setTimeout 150)))))}]

     ;; No search has been entered -- show the splash.
     (if (string/blank? q) [splash]
       ;; A search was entered and there might be results.
       (e/search-results displays "No results" results))]))
