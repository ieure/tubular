;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.display.subs
  (:require [re-frame.core :as rf]
            [tubular.spec.db :as db]))

;; All display data
(rf/reg-sub
 ::displays
 (fn [{{:keys [::db/displays]} ::db/data :as db}]
   displays))
