;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.display.views
  (:require [re-frame.core :as re-frame]
            [tubular.data :as data]
            [tubular.display :as display]
            [tubular.subs :as subs]))


(defn render [{:keys [id]}]
  [:h1 "Display " id])
