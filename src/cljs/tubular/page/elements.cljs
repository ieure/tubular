;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.elements
  (:require [bulma-cljs.core :as b]
            [re-frame.core :as re-frame]
            [taoensso.timbre :as timbre :refer-macros [infof]]
            [tubular.data :as data]
            [tubular.display :as display]
            [tubular.events :as events]
            [tubular.routes :as routes]
            [tubular.slug :as slug]
            [tubular.spec.db :as db]
            [tubular.spec.search :as ss]
            [tubular.spec.tube :as stube]))

(defn heading [text]
  [:h2 {:class "title is-4 subheading"} text])

(defn tube-link [id]
  [:a {:href (routes/url-for ::db/tube ::stube/desig id)} id])

(defn tube-xref-link [id]
  [:a {:href (routes/url-for ::db/tube-xref ::stube/desig id)
       :class "button is-info is-size-5"}
   "⤮ Swap Candidates"])

(defn display-link "Render a link to a display."
  [[slug display :as slug+display] & format]
  (let [format (or format [:manufacturer :model])]
    (if-let [url (apply routes/url-for [:display :id slug])]
      [:a {:href url}
       (apply display/describe display format)]
      (apply display/describe display format))))

 ;; Searching

(defn search-result "Render a single search result."
  [displays [type id]]
  ^{:key (str "sr-" id)}
  [:li {:class "is-size-5 is-block"}
   (condp = type
     ::ss/display (display-link displays id)
     ::ss/tube (tube-link id))])

(defn search-results "Render search results"
  [displays no-results-text results]
  (if (empty? results)
    [:ul {:class "search-results"}
     ^{:key "no-results"} [:li {:class "no-results"} no-results-text]]
    [:ul {:class "search-results is-hoverable is-striped"}
     (for [result results] [search-result displays result])]))
