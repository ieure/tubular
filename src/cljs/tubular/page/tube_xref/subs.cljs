;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.tube-xref.subs
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [re-frame.core :as rf]
            [taoensso.timbre :as timbre :refer-macros [debugf infof]]
            [tubular.page.display.subs :as display]
            [tubular.page.tube.subs :as tube]
            [tubular.spec.db :as db]
            [tubular.spec.display :as sdisplay]
            [tubular.spec.tube :as stube]
            [tubular.spec.tube.compat :as scompat]
            [tubular.subs :as subs]
            [tubular.tube.compat :as compat]
            [tubular.tube.parser :as parser]))

(rf/reg-sub
 ::direct-replacements
 (fn []
   [(rf/subscribe [::subs/data])
    (rf/subscribe [::tube/desig])])

 (fn [[{:keys [::db/equiv]} desig]]
   (-> (reduce set/union (filter #(contains? % desig) equiv))
       (disj desig))))

(rf/reg-sub
 ::swap-candidates
 (fn []
   [(rf/subscribe [::subs/tubes])
    (rf/subscribe [::tube/tube])])

 (fn [[tubes [desig parsed-desig tube]]]
   (compat/all-compat tubes parsed-desig tube)))

(rf/reg-sub
 ::xref
 (fn [] [(rf/subscribe [::tube/desig])
         (rf/subscribe [::direct-replacements])
         (rf/subscribe [::swap-candidates])])
 (fn [[desig directs candidates]]
   (let [ds (into #{} (cons desig directs))]
   [directs
    (remove (fn [[desig _]] (contains? ds desig))
            candidates)])))
