;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.tube-xref.views
  (:require [clojure.core.match :refer [match]]
            [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [re-frame.core :as rf]
            [taoensso.timbre :as timbre :refer-macros [infof]]
            [tubular.page.elements :as e]
            [tubular.page.tube-xref.subs :as subs]
            [tubular.page.tube.subs :as tube]
            [tubular.routes :as routes]
            [tubular.spec.db :as sdb]
            [tubular.spec.tube :as stube]
            [tubular.spec.tube.compat :as scompat]
            [tubular.tube.compat :as compat]))

(defn render-compat [report type]
  (condp = (get report type)
    ::scompat/compat ^{:key "tcompat"} [:td {:class "is-success"} "✓"]
    ::scompat/maybe ^{:key "tmaybe"} [:td {:class "is-warning"} "?"]))


(defn render []
  (let [desig @(rf/subscribe [::tube/desig])
        [directs candidates] @(rf/subscribe [::subs/xref])
        grpd (group-by (fn [[desig report]]
                         (compat/describe report)) candidates)]

    [:div
     [:nav {:class "breadcrumb mt-2" :aria-label "breadcrumbs"}
      [:ul
       [:li [:a {:href (routes/url-for ::sdb/home)} "Home"]]
       [:li [:a {:href (routes/url-for ::sdb/tube ::stube/desig desig)} desig]]
       [:li {:class "is-active"} [:a {:href "#" :aria-current "page"} "Swap Candidates"]]]]
     [:h1 {:class "has-text-info tube-heading mb-2 mt-4"}
      "Swap Candidates for " desig]
     [:p {:class "mb-5"} "These tubes might be suitable replacements, but success isn't guaranteed."]
     [:div {:class "columns is-mobile"}
      (for [[desc scands] grpd]
        [:div {:class "column is-half"}
         [:ul {:class "search-results is-hoverable is-striped"}
          [e/heading desc]
          (for [[swap-desig report] scands]
            ^{:key (str "sc-" desig "-" swap-desig)}
            [:li {:class "is-size-4 is-block"} (e/tube-link swap-desig)])]])]]))
