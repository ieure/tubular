;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.tube.subs
  (:require [clojure.string :as string]
            [re-frame.core :as rf]
            [taoensso.timbre :as timbre :refer-macros [debugf infof]]
            [tubular.config :as config]
            [tubular.search :as search]
            [tubular.spec.db :as db]
            [tubular.spec.tube :as stube]
            [tubular.spec.tube.compat :as scompat]
            [tubular.subs :as subs]
            [tubular.tube.compat :as tc]
            [tubular.tube.compat :as compat]
            [tubular.tube.parser :as parser]))

;; ID of the selected tube.
(rf/reg-sub
 ::desig
 :<- [::subs/panel+args]
 (fn [[panel {:keys [::stube/desig] :as args}]]
   desig))

(rf/reg-sub
 ::parsed-desig
 (fn [] [(rf/subscribe [::desig])])
 (fn [[desig]]
   (parser/parse desig)))

;; Data for the selected tube
(rf/reg-sub
 ::tube
 (fn [] [(rf/subscribe [::desig]), (rf/subscribe [::parsed-desig]), (rf/subscribe [::subs/tubes])])
 (fn [[desig parsed tubes]]
   [desig parsed (get tubes desig)]))
