;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.page.tube.views
  (:require [clojure.core.match :refer [match]]
            [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [re-frame.core :as rf]
            [taoensso.timbre :as timbre :refer-macros [infof]]
            [tubular.asset :as asset]
            [tubular.config :as config]
            [tubular.data :as data]
            [tubular.page.elements :as e]
            [tubular.page.home.views :as home]
            [tubular.page.tube-xref.subs :as xsubs]
            [tubular.page.tube.subs :as subs]
            [tubular.phosphor :as phos]
            [tubular.routes :as routes]
            [tubular.spec.db :as sdb]
            [tubular.spec.rejuv.bk :as sbk]
            [tubular.spec.search :as ss]
            [tubular.subs :as common-subs]
            [tubular.tube.parser :as parser]
            [tubular.tube.size :as sizes]))

(defn has-bk? "Does this tube have BK rejuvenator setup data?"
  [{:keys [:bk] :as setup}] bk)

(defn has-sencore? "Does this tube have Sencore rejuvenator setup data?"
  [{:keys [:cr31 :cr70 :cr7000] :as setup}] (or cr31 cr70 cr7000))

(defn has-rejuvenator? "Does this tube have any rejuvenator setup data?"
  [setup] (or (has-bk? setup) (has-sencore? setup)))



(defn render-details "Render detailed breakdown of the tube, from its parsed designation."
  [{:keys [:system :size :region :application :family :family-member :phosphor :yoke :bonded-yoke] :as parsed}]
  [:table {:class "table is-bordered is-striped is-hoverable is-fullwidth"}
   [:caption "Designation"]
   [:tbody
    [:tr [:th "System"]   [:td (get parser/systems system "Unknown")]]
    [:tr [:th "Region"]   [:td (get parser/regions region "Unknown")]]
    (when application
      [:tr [:th "Application"]   [:td application]])
    [:tr [:th "Size"]     [:td (sizes/describe size)]]
    [:tr [:th "Family"]   [:td family]]
    (when family-member
      [:tr [:th "Family Member"]   [:td family-member]])
    [:tr [:th "Phosphor"] [:td (phos/describe system phosphor)]]
    (when yoke
      [:tr [:th "Yoke"]   [:td yoke]])
    (when bonded-yoke
      [:tr [:th "Bonded Yoke"]   [:td bonded-yoke]])]])

(defn render-bk-adapter [prefix n]
  (let [ds [:bkcr n]
        text (asset/link-text ds)]
    ^{:key (str "bk-" desc)}
    (if-let [url (asset/datasheet ds)]
      [:a {:class "is-block" :href url} text]
      text)))

(defn render-bk-setup "Render BK Precision rejuvenator setup info."
  [{{:keys [:options]} :as bk, :bk}]
  [:table {:class "table is-bordered is-striped is-hoverable is-fullwidth has-text-centered"}
   [:caption "BK Precision"]
   [:thead
    [:tr [:th "Model"] [:th "Adapter"] [:th "Filament"] [:th "G1"]]]
    (into []
          (concat
           [:tbody]
           (match (s/conform ::sbk/bk bk)
                  [:normal {:adapter a :filament f :g1 g}]
                  [[:tr [:th "467-490"] [:td (render-bk-adapter "CR" a)] [:td f] [:td g]]
                   [:tr [:th "490B"]    [:td (str "CA-" a)] [:td "—"]      [:td "—"]]],

                  [:special {:adapter a :filament f :g1-467&90 g :g1-470&80 gg}]
                  [[:tr [:th "467 & 490"] [:td (render-bk-adapter "CR" a)] [:td f]   [:td g]]
                   [:tr [:th "470 & 480"] [:td (render-bk-adapter "CR" a)] [:td "—"] [:td gg]]
                   [:tr [:th "490B"]      [:td (str "CA-" a)] [:td "—"] [:td g]]])

           (when options
             [[:tr [:th "Notes"] [:td {:colSpan 4} (get sbk/options options)]]])))])

(defn render-sencore-setup "Render Sencore rejuvenator setup info."
  [{:keys [:cr31 :cr70 :cr7000]}]
  [:table {:class "table is-bordered is-striped is-hoverable is-fullwidth has-text-centered"}
   [:caption "Sencore"]
   [:thead
    [:tr [:th "Model"] [:th "Adapter"] [:th "Bias"] [:th {:colSpan 2} "Type"]]]
   [:tbody
    (when-let [{:keys [:socket :bias]} cr31]
      [:tr [:th "CR31"]   [:td socket]   [:td (string/upper-case (subs (str bias) 1))]    [:td "n/a"]])
    (when-let [{:keys [:socket :bias :type]} cr70]
      [:tr [:th "CR70"]   [:td socket]   [:td bias] [:td type]])
    (when-let [{:keys [:socket :bias :type]} cr7000]
      [:tr [:th "CR7000"] [:td socket]   [:td bias] [:td type]])]])

(defn render-voltage [{:keys [:filament :cathode :anode :focus :g2]} direct-heat]
  [:table {:class "table is-bordered is-striped is-hoverable is-fullwidth"}
   [:caption "Voltages"]
   [:tbody
    (when filament [:tr [:th "Filament"]    [:td filament]])
    [:tr [:th "Direct Heat"] [:td (if direct-heat "Yes" "No")]]
    (when cathode [:tr [:th "Cathode"]     [:td cathode]])
    (when anode [:tr [:th "Anode"]       [:td anode]])
    (when focus [:tr [:th "Focus"]       [:td focus]])
    (when g2 [:tr [:th "G2 (Max)"]    [:td g2]])]])

(defn render-pinout [{:keys [:f1 :f2 :r :g :b]}]
  [:table {:class "table is-bordered is-striped is-hoverable is-fullwidth has-text-centered"}
   [:caption "Neck Pinout"]
   [:thead
    [:tr [:td] [:th {:title "Cathode"} "K" ] [:th {:title "Control Grid"} "G1"] [:th {:title "Screen Grid"} "G2"]]]

   [:tbody
    (when-let [{:keys [:k :g1 :g2]} r]
      [:tr {:class "red"}   [:th {:class "has-text-right"}"Red"]   [:td k]  [:td g1] [:td g2]])
    (when-let [{:keys [:k :g1 :g2]} g]
      [:tr {:class "green"} [:th {:class "has-text-right"} "Green"] [:td k]  [:td g1] [:td g2]])
    (when-let [{:keys [:k :g1 :g2]} b]
      [:tr {:class "blue"}  [:th {:class "has-text-right"} "Blue"]  [:td k]  [:td g1] [:td g2]])

    [:tr [:td  {:colSpan 2}]    [:th "F1"] [:th "F2"]]
    (when (and f1 f2)
      [:tr [:th {:class "has-text-right"} "Filament"] [:td] [:td f1]   [:td f2]])]])

(defn render-datasheets [datasheets]
  (let [gds (group-by asset/datasheet-type datasheets)]
    `[:table {:class "datasheet table is-bordered is-striped is-fullwidth has-text-centered"}
      [:thead
       [:caption ~(str "Datasheet" (if (> (count datasheets) 1) "s" ""))]]
      ~@(for [[grp specs] gds]
          [:tbody
           (for [[i ds-spec] (->> specs sort (interleave (range)) (partition 2))]
             [:tr
              ;; Include group name on the 0th datasheet spec
              (when (zero? i) [:th {:rowspan (count specs)} (asset/datasheet-type-text grp)])
                 [:td
                  [:a {:class "is-block" :href (asset/datasheet ds-spec)}
                   (asset/link-text ds-spec)]]])])]))

(defn render []
  (let [[desig {:keys [:phosphor] :as parsed},
         {:keys [:voltage :direct-heat :pinout :datasheets] :as tube}]
        @(rf/subscribe [::subs/tube])

        directs @(rf/subscribe [::common-subs/equiv desig])
        displays @(rf/subscribe [::common-subs/displays desig])]

    ^{:key (str "tube/" desig)}
    [:div {:class "tube"}
     [:nav {:class "breadcrumb mt-2" :aria-label "breadcrumbs"}
      [:ul [:li [:a {:href (routes/url-for ::sdb/home)} "Home"]]
       [:li {:class "is-active"} [:a {:href "#" :aria-current "page"} desig]]]]
     [:h1 {:class "has-text-info tube-heading mb-5"}
      desig " " (if (phos/color? phosphor) "Color" "B&W") " Picture Tube"]

     ;; Tube breakdown (system, region, phosphor etc)
     [e/heading "Specifications"]
     [:div {:class "columns"}
      [:div {:class "column"} [render-details parsed]]

      (when voltage
        [:div {:class "column"} [render-voltage voltage direct-heat]])

      (when pinout
        [:div {:class "column"} [render-pinout pinout]])]

     (when datasheets
      [:div {:class "columns"}
        [:div {:class "column"} [render-datasheets datasheets]]])

     (when (has-rejuvenator? tube)
       [:div
        [e/heading "Rejuvenator Setup"]
        [:div {:class "columns"}
         (when (has-bk? tube)      [:div {:class "column"} [render-bk-setup tube]])
         (when (has-sencore? tube) [:div {:class "column"} [render-sencore-setup tube]])]])

     [:div {:class "columns mb-4"}
      [:div {:class "column"}
       [e/heading "Cross-Reference"]
       (if-not (empty? directs)
         [:div
          [:p "These tubes are fully compatible substitutes for a " desig "."]
          (e/search-results nil "" (map (fn [desig] [::ss/tube desig]) directs))
          [:br]
          [:p "You can also look for more candidates which might work, but success isn't guaranteed."]]

         ;; Nothing found
         [:div
          [:p "No compatible substitutes known, but a swap candidate may work."]])

       [:div {:class "has-text-centered"} [:br] [e/tube-xref-link desig]]]

      (when-not (empty? displays)
        [:div {:class "column"}
         [e/heading "Used In"]
         (binding [tubular.display/*format* [:manufacturer :model]]
           ^{:key (str desig "-used-in")} [:ul {:class "search-results is-hoverable is-striped"}
            (for [[slug _ :as s+d] displays]
              ^{:key (str desig "-" slug)}
              [:li {:class "is-size-5 is-block"} (e/display-link s+d)])])])]]))
