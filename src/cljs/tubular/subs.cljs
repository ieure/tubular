;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.subs
  "Re-frame subscriptions."
  (:require [clojure.set :as set]
            [clojure.spec.alpha :as s]
            [re-frame.core :as rf]
            [taoensso.timbre :as timbre :refer-macros [debugf infof]]
            [tubular.spec.db :as sdb]
            [tubular.tube.compat :as tc]))

;; Extract all data from the DB.
(rf/reg-sub
 ::data
 (fn [{:keys [::sdb/data] :as db}]
   data))

;; Extract the current panel and any arguments.
;; The panel is a vector of [view-sym arg-map?].
(rf/reg-sub
 ::panel+args
 (fn [{:keys [::sdb/panel] :as db}]
   {
    ;; :pre [(s/assert ::sdb/db db)]
    :post [(s/assert ::sdb/panel %)]}
   panel))

;; Extract the panel only.
;; This is used by tubular.views/main-panel to delegate to the
;; appropriate render fn.
(rf/reg-sub
 ::panel
 :<- [::panel+args]
 (fn [[panel & _] _]
   panel))

;; All tube data
(rf/reg-sub
 ::tubes
 (fn [{{:keys [::sdb/tubes]} ::sdb/data :as db}]
   tubes))

;; All displays
(rf/reg-sub
 ::displays
 (fn
   ([{{:keys [::sdb/displays]} ::sdb/data :as db}]
    displays)

   ;; Displays which use a particular tube.
   ([{{:keys [::sdb/displays]} ::sdb/data :as db} [_ desig]]
    (tc/used-in displays desig))))

;; All equivalent tubes
(rf/reg-sub
 ::equiv
 (fn
   ([{{:keys [::sdb/equiv]} ::sdb/data :as db}]
    equiv)
   ([{{:keys [::sdb/equiv]} ::sdb/data :as db} [_ desig]]
     (-> (reduce set/union (filter #(contains? % desig) equiv))
         (disj desig)))))
