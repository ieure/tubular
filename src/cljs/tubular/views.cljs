;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.views
  "Primary controller for views."
  (:require
   [bulma-cljs.core :as b]
   [re-frame.core :as re-frame]
   [taoensso.timbre :as timbre :refer-macros [debugf]]
   [tubular.events :as events]
   [tubular.page.display.views :as display]
   [tubular.page.home.views :as home]
   [tubular.page.tube-xref.views :as tube-xref]
   [tubular.page.tube.views :as tube]
   [tubular.routes :as routes]
   [tubular.spec.db :as db]
   [tubular.subs :as subs]))

(def views
  "Map of syms to render functions."
  {::db/loading      (fn [] nil)
   ::db/home         home/render

   ::db/tube         tube/render
   ::db/tube-xref    tube-xref/render

   ::db/display      display/render
   ::db/display-xref display/render})

(defn main-panel
  "Primary view handler.

  This delegates to the appropriate render function for the current
  panel, using the ::subs/panel subscription and the views map."
  []
  (let [panel @(re-frame/subscribe [::subs/panel])]
    (debugf "main-panel panel=%s" panel)
    (if-let [view (get views panel)]
      [view]
      [:h1 "Internal error, no view for panel '" panel "'"])))
