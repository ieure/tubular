;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.tube.compat-test
  (:require [clojure.test :as t]
            [tubular.spec.db :as db]
            [tubular.spec.tube.compat :as tc]
            [tubular.test.data :refer [data]]
            [tubular.tube.compat :as sut]
            [tubular.tube.parser :as p]))

(def tubes (::db/tubes data))

(def displays (::db/displays data))

(t/deftest test-pinout-compat?
  (let [a (get tubes "19VLUP22")
        b (get tubes "19VDJP22")
        c (get tubes "19VARP4")]
  (t/is (= ::tc/compat (sut/pinout-compat? [nil a] [nil b])))
  (t/is (= ::tc/incompat (sut/pinout-compat? [nil c] [nil b])))))

(t/deftest test-size-compat?
  (t/is (= ::tc/compat (sut/size-compat? [(p/parse "19VLUP22") nil]
                                         [(p/parse "19VDJP22") nil])))
  (t/is (= ::tc/compat (sut/size-compat? [(p/parse "19VLUP22") nil]
                                        [(p/parse "A48AAB10X") nil]))))

(t/deftest test-compat?
  (let [a "19VLUP22"
        b "19VDJP22"]
    (t/is (= {::tc/size ::tc/compat, ::tc/pinout ::tc/compat}
             (sut/compat? [(p/parse a) (get tubes a)]
                          [(p/parse b) (get tubes b)])))))

(t/deftest test-simple-compat?
  (let [a "19VLUP22"
        b "19VDJP22"
        c "13VDJP22"]
    (t/is (= ::tc/compat (sut/simple-compat?
                          (sut/compat? [(p/parse a) (get tubes a)]
                                       [(p/parse b) (get tubes b)]))))
    (t/is (= ::tc/incompat (sut/simple-compat?
                            (sut/compat? [(p/parse a) (get tubes a)]
                                         [(p/parse c) (get tubes c)]))))))

(t/deftest test-all-compat
  (let [td "19VLUP22"]
    (t/is (= "19VDJP22"
             (first (map first (sut/all-compat tubes (p/parse td) (get tubes td))))))))

(t/deftest test-compat>
  (let [a "19VLUP22"
        ad [(p/parse a) (get tubes a)]
        b "19VJTP22"
        bd [(p/parse b) (get tubes b)]
        c1 (sut/compat? ad bd)
        c2 (sut/compat? bd ad)]

    (t/is (= c1  c2))
    (t/is (sut/compat> [a c1] [b c2]))))

(t/deftest test-used-in?
  (t/is (sut/used-in? (get displays "wells-gardner-19-k6100") "19VLUP22")))

(t/deftest test-used-in
  (let [slugs ["electrohome-19-g08-003" "wells-gardner-19-k6100"]
        disps (into [] (select-keys displays slugs))]
    (t/is (= disps
             (sut/used-in displays "19VLUP22")))))

(t/deftest test-describe
  (t/is (= "Wrong size" (sut/describe {::tc/size ::tc/incompat})))
  (t/is (= "Wrong pinout" (sut/describe {::tc/pinout ::tc/incompat})))
  (t/is (= "Unknown" (sut/describe {::tc/size ::tc/unknown})))
  (t/is (= "Unknown" (sut/describe {::tc/pinout ::tc/unknown}))))
