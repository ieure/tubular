;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2013-2022 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.tube.parser-test
  (:require [clojure.test :refer [deftest testing is]]
            [tubular.spec.db :as db]
            [tubular.test.data :as tdata]
            [tubular.tube.parser :as tube]
            [tubular.tube.size :refer [->Size]]))

(defonce wtds-tube "A48AAB00X")
(defonce eia-tube "19VLUP22")
(defonce eiaj-tube "3708B22")
(defonce pro-electron-tube "A37-554X")

(defn test-one [func t]
  (let [clean (tube/cleanup t)]
    (testing clean (func clean))))

(defn test-parse [name func good bad]
  (testing name
    (doseq [t good]
      (testing (str "good: " t)
        (is (test-one func t))))
    (doseq [t bad]
      (testing (str "bad: " t)
        (is (not (test-one func t)))))))

(deftest wtds
  (testing wtds-tube
    (test-parse "WTDS" tube/wtds [wtds-tube] [eia-tube eiaj-tube pro-electron-tube])
    (is (= {:system :wtds
            :application "Direct view TV"
            :size (->Size 48 :cm :viewable)
            :family "AAB"
            :phosphor :x
            :region :usa
            :family-member "00"}
           (tube/wtds wtds-tube))))

  (testing "M48JKE8WW"
    (is (= :w (:phosphor (tube/wtds "M48JKE8WW")))))

  (testing "M51JPV005WZ"
    (is (= :w (:phosphor (tube/wtds "M51JPV005WZ"))))))

(deftest eia
  (testing eia-tube
    (test-parse "EIA" tube/eia [eia-tube] [wtds-tube eiaj-tube pro-electron-tube])
    (is (= {:system :eia
            :region :usa
            :family "LU"
            :phosphor :p22
            :size (->Size 19 :in :viewable)}
           (tube/eia eia-tube)))))

(deftest eiaj
  (testing eiaj-tube
    (test-parse "EIAJ" tube/eiaj [eiaj-tube] [wtds-tube eia-tube pro-electron-tube])
    (is (= {:system :eiaj
            :region :japan
            :family "8"
            :phosphor :p22
            :size (->Size 370 :mm :face)}
           (tube/eiaj eiaj-tube)))))

(deftest pro-electron
  (testing pro-electron-tube
    (test-parse "Pro Electron" tube/pro-electron [pro-electron-tube] [wtds-tube eia-tube eiaj-tube])
    (is (= {:system :pro-electron
            :region :europe
            :application "Direct view TV tube"
            :size (->Size 37 :cm :face)
            :family "554"
            :phosphor :x}
           (tube/pro-electron pro-electron-tube)))))

(deftest test-general-parsing
  (is (= :wtds (:system (tube/parse wtds-tube))))
  (is (= :eia (:system (tube/parse eia-tube))))
  (is (= :eiaj (:system (tube/parse eiaj-tube))))
  (is (= :pro-electron (:system (tube/parse pro-electron-tube)))))

(deftest test-all-parse
  (doseq [desig (keys (::db/tubes tdata/data))]
    (testing desig
      (is (tube/parse desig)))))
