;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2013-2021 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.tube.sizes-test
  (:require [clojure.test :refer [deftest testing is]]
            [tubular.tube.size :as s]))

(def s19iv (s/->Size 19 :in :viewable))
(def s13iv (s/->Size 13 :in :viewable))
(def s15iv (s/->Size 15 :in :viewable))
(def s25iv (s/->Size 25 :in :viewable))
(def s20if (s/->Size 20 :in :face))

(def s34cv (s/->Size 34 :cm :viewable))
(def s48cv (s/->Size 48 :cm :viewable))
(def s63cv (s/->Size 63 :cm :viewable))
(def s68cv (s/->Size 68 :cm :viewable))

(def s370mf (s/->Size 370 :mm :face))
(def s510mf (s/->Size 510 :mm :face))
(def s50cf  (s/->Size 50 :cm :face))

(deftest test-inches
  (is (= 13 (-> s34cv s/->in :size)))
  (is (= 14 (-> s370mf s/->in :size)))
  (is (= 19 (-> s48cv s/->in :size)))
  (is (= 20 (-> s50cf s/->in :size)))
  (is (= 25 (-> s63cv s/->in :size)))
  (is (= 27 (-> s68cv s/->in :size))))

(deftest test-mm
  (is (= 480 (-> s48cv s/->mm :size)))
  (is (= 482 (-> s19iv (s/->mm) :size)))
  (is (= 500 (-> s50cf (s/->mm) :size))))

(deftest test-viewable
  (is (= 13 (-> s370mf s/->in s/->viewable :size)))
  (is (= 19 (-> s20if  s/->in s/->viewable :size)))
  (is (= 19 (-> s50cf  s/->in s/->viewable :size))))

(deftest size-accessor
  (is (= 19 (:size s19iv)))
  (is (= 50 (:size s50cf))))

(deftest test-compat
  (is (s/compat? s370mf s13iv))
  (is (s/compat? s48cv  s19iv))
  (is (s/compat? s19iv  s20if))
  (is (s/compat? s19iv  s50cf))
  (is (s/compat? s510mf s19iv)))
