;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.routes-test
  (:require
   [#?(:clj clojure.test, :cljs cljs.test)
    #?(:clj :refer, :cljs :refer-macros)
    [deftest is]]
   [tubular.routes :as sut]
   [tubular.spec.db :as db]
   [tubular.spec.display :as sdisp]
   [tubular.spec.tube :as stube]))

(deftest test-parse-home
  (is (= {:handler :tubular.spec.db/home}
         (sut/parse "/"))))

(deftest test-url-for-home
  (is (= "/" (sut/url-for ::db/home))))

(deftest test-unparse-home
  (is (= "/" (sut/unparse {:handler :tubular.spec.db/home}))))

(deftest test-parse-tube
  (is (= {:handler :tubular.spec.db/tube
          :route-params {::stube/desig "19vlup22"}}
         (sut/parse "/tube/19vlup22"))))

(deftest test-url-for-tube
  (is (= "/tube/19vlup22"
         (sut/url-for ::db/tube
                      ::stube/desig "19vlup22"))))

(deftest test-unparse-tube
  (is (= "/tube/19vlup22"
         (sut/unparse {:handler :tubular.spec.db/tube
                       :route-params #:tubular.spec.tube{:desig "19vlup22"}}))))

(deftest test-parse-tube-xref
  (is (= {:handler :tubular.spec.db/tube-xref
          :route-params {::stube/desig "19vlup22"}}
         (sut/parse "/xref/tube/19vlup22"))))

(deftest test-url-for-tube-xref
  (is (= "/xref/tube/19vlup22"
         (sut/url-for :tubular.spec.db/tube-xref ::stube/desig "19vlup22"))))

(deftest test-unparse-tube-xref
  (is (= "/xref/tube/19vlup22"
         (sut/unparse {:handler :tubular.spec.db/tube-xref
                       :route-params {::stube/desig "19vlup22"}}))))

#_(deftest test-parse-display
  (is (= {:handler :tubular.spec.db/display
          :route-params {::sdisp/slug "wells-gardner-19-k6100"}}
         (sut/parse "/display/wells-gardner-19-k6100"))))

#_(deftest test-url-for-display
  (is (= "/display/wells-gardner-19-k6100"
         (sut/url-for ::db/display
                      ::sdisp/slug "wells-gardner-19-k6100"))))

#_(deftest test-unparse-display
  (is (= "/display/wells-gardner-19-k6100"
         (sut/unparse {:handler :tubular.spec.db/display
                       :route-params {::sdisp/slug "wells-gardner-19-k6100"}}))))

#_(deftest test-parse-display-xref
  (is (= {:handler :tubular.spec.db/display-xref
          :route-params {::sdisp/slug "wells-gardner-19-k6100",
                         ::stube/xref "a48"}}
         (sut/parse "/display/wells-gardner-19-k6100/xref/a48"))))

#_(deftest test-url-for-display-xref
  (is (= "/display/wells-gardner-19-k6100/xref/a48"
         (sut/url-for ::db/display
                      ::sdisp/slug "wells-gardner-19-k6100",
                      ::stube/xref "a48"))))

#_(deftest test-unparse-display-xref
  (is (= "/display/wells-gardner-19-k6100/xref/a48"
           (sut/unparse {:handler :tubular.spec.db/display-xref
                         :route-params {::sdisp/slug "wells-gardner-19-k6100",
                                        ::stube/xref "a48"}}))))
