;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.test.spec
  (:require [clojure.spec.alpha :as s]
            [clojure.test :as t]))

(defmacro valid [spec x]
  `(t/is (s/valid? ~spec ~x)
      (s/explain-str ~spec ~x)))
