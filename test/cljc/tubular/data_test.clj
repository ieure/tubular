;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure
;; Author: Ian Eure <ian.eure@gmail.com>
;;
(ns tubular.data-test
  (:require [clojure.test :refer :all]
            [tubular.spec.db :as db]
            [tubular.spec.display :as sdisp]
            [tubular.spec.tube :as stube]
            [tubular.test.data :as tdata]
            [tubular.test.spec :as tspec]))

(deftest test-tubes-conform
  (doseq [[desig tube] (::db/tubes tdata/data)]
    (testing desig
      (tspec/valid ::stube/tube tube))))

(deftest test-displays-conform
  (doseq [[slug disp] (::db/displays tdata/data)]
    (testing slug
      (tspec/valid ::sdisp/display disp))))
