;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.title-test
  (:require #?(:clj [clojure.test :as t]
               :cljs [cljs.test :as t :include-macros true])
            [tubular.spec.db :as sdb]
            [tubular.spec.tube :as stube]
            [tubular.title :as sut]))

(t/testing "title"
  (t/is (= "Tubular — 19VLUP22"
           (sut/title [::sdb/tube {::stube/desig "19VLUP22"}])))
  (t/is (= "Tubular — 19VLUP22 — Cross-reference"
           (sut/title [::sdb/tube-xref {::stube/desig "19VLUP22"}]))))
