;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2013-2021 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.math-test
  (:require [clojure.test :refer [deftest testing is]]
            [tubular.math :as math]))

(deftest test-abs
  (is (= 5 (math/abs 5)))
  (is (= 5 (math/abs -5))))
