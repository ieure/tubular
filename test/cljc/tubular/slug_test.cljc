;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2021 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.slug-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [tubular.slug :refer [slug]]
   [tubular.data :as data]))

(deftest test-slug
  (is (= "amplifone-13-vector"
         (slug {:tubes #{"13VBSP22"},
                :color true,
                :application :game,
                :docs
                {"Service Manual"
                 "http://arcarc.xmission.com/PDF_Monitors/Atari%20Monitor%20TM-222%201st%20Printing%20Amplifone%2013in%20Color%20XY.pdf"},
                :scan :vector,
                :notes
                "Unique PCB, includes integrated deflection & HV on a single board. Has a neck PCB.",
                :manufacturer "Amplifone",
                :yoke {:id "A201075-01 100310T", :h 0.8, :v 0.6},
                :model "Vector"}))))
