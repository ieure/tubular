;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.spec.db-test
  (:require  [clojure.spec.alpha :as s]
             [clojure.test :refer :all]
             [tubular.spec.db :as db]
             [tubular.spec.tube :as stube]
             [tubular.test.spec :as ts]))

(deftest test-home
  (ts/valid ::db/home [::db/home]))

(deftest test-tube-panel
  (ts/valid ::db/tube  [::db/tube {::stube/desig "19vlup22"}])
  (ts/valid ::db/panel [::db/tube {::stube/desig "19vlup22"}])

  (ts/valid ::db/tube-xref [::db/tube-xref {::stube/desig "19vlup22", ::stube/xref "a48"}])
  (ts/valid ::db/panel     [::db/tube-xref {::stube/desig "19vlup22", ::stube/xref "a48"}]))
