;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.search-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [tubular.data :as data]
   [tubular.search :as search]))

(deftest test-regex?
  (is (search/regex? "foo.*bar"))
  (is (search/regex? "^19v"))
  (is (not (search/regex? "lup")))
  (is (search/regex? "p22$"))
  (is (not (search/regex? "foo bar"))))

(deftest test-matcher-for-string
  (let [st "19vl"
        m (search/matcher-for st)]
    (is (not (search/regex? st)))
    (is (m "19vlup22"))
    (is (m "19VLUP22"))
    (is (not (m " 19VLUP22")))))

(deftest test-matcher-for-regex
  (let [st "1[39]vlup"
        m (search/matcher-for st)]
    (is (search/regex? st))
    (is (m "19vlup22"))
    (is (m "19VLUP22"))
    (is (m "13vlup22"))
    (is (m "13VLUP22"))
    (is (m " 19VLUP22"))))

(deftest test-matcher-for
  (testing "Regular expressions"
    (let [m (search/matcher-for "1[39]vlup")]
      (is (m "19vlup22"))
      (is (m "19VLUP22"))
      (is (m "13vlup22"))
      (is (m "13VLUP22"))
      (is (m " 19VLUP22"))))

  (testing "Text"
    (let [m (search/matcher-for "19vl")]
      (is (m "19vlup22"))
      (is (m "19VLUP22"))
      (is (not (m " 19VLUP22"))))))

#_(deftest test-search-tubes
  (is (= '([:tube "19VLUP22"]) (search/tubes "19vlup22")))
  (is (= '([:tube "19VLUP22"]) (search/tubes "19vlup"))))

#_(deftest test-search-displays
  (testing "By name"
  (is (= '([:display "electrohome-13-g07-fbo"] [:display "electrohome-19-g07-cbo"])
         (sort (search/displays "g07")))))

  (testing "By tube"
    (is (= '([:display "electrohome-19-g08-003"]
             [:display "wells-gardner-19-k6100"])
           (sort (search/displays "19vlup22"))))))
