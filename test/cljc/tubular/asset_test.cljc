;; -*- coding: utf-8 -*-
;;   __         __          __
;;  |  |_.--.--|  |--.--.--|  |---.-.----.
;;  |   _|  |  |  _  |  |  |  |  _  |   _|
;;  |____|_____|_____|_____|__|___._|__|
;;
;; © 2012-2022 Ian Eure.
;; Author: Ian Eure <ian@lowbar.fyi>
;;
(ns tubular.asset-test
  (:require #?(:clj [clojure.test :as t]
               :cljs [cljs.test :as t :include-macros true])
            [tubular.asset :as sut]
            [tubular.config :as config]
            [tubular.spec.asset :as sasset]))


(t/deftest datasheet-type
  (t/is ::sasset/bk-cr (sut/datasheet-type [:bkcr 31]))
  (t/is ::sasset/jedec-release (sut/datasheet-type [:jedec "1234"]))
  (t/is ::sasset/generic-crt (sut/datasheet-type [:crt "19VLUP22"])))

(t/deftest test-datasheet
  (t/is (sut/datasheet [:bkcr 31]))
  (t/is (sut/datasheet [:jedec "1234"]))
  (t/is (sut/datasheet [:crt "19VLUP22"])))

(t/deftest test-datasheet-bkcr
  (let [an 999]
    (t/is (nil? (sut/datasheet [:bkcr an]))))

  (let [an 31]
    (t/is (sut/datasheet [:bkcr an]))))
