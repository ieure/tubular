FROM nginx:alpine
LABEL org.opencontainers.image.authors="ian@lowbar.fyi"
COPY resources/public /usr/share/nginx/html
COPY resources/nginx.conf /etc/nginx/templates/tubular.conf.template
